import argparse
import logging
import os
import os.path
import pprint
from compare_files_and_dirs import handle_cmdline, get_dir_list, complete_file_chk, dir_exist_chk, email_results, logsetup
from fnmatch import fnmatch


def handle_cmdline():
    """Parse user input into 2 paths to compare for presence of links.
    """

    usage = """Generate a list of links expected to be found in the second
    location provided from the path of the first location.
    """

    # read in command line and options
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(dest='paths', metavar='directory_name', nargs=2,
                        help='Full path to dir w/o trailing slash.\
                              Can use "." for first path.')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    if args.paths[0] == '.':  # to allow use of '.' for cwd
        args.paths[0] = os.getcwd()
    return args.paths, log_level  # , args.outfile


def get_project_links(list_of_paths, firstpath, secondpath):
    '''Take list of directories with complete file, constructs and returns
    expected link paths to be checked from paths of provided directories.
    '''
    list_of_links = []
    dirs_not_checked = []
    logging.debug('Start get_project_links.')
    for dirname in list_of_paths:  # flowcell dir w complete file
        fullpath = firstpath + '/' + dirname  # /coldstorage/fastq/flowcell 
        for proj_dirname in os.listdir(fullpath):  
            PIandsample = proj_dirname[8:]  # Strips 'Project_'
            PIname = PIandsample.split('-',1)[0]  # Strip sample name
            
            if os.path.isdir(os.path.join(fullpath, proj_dirname))\
               and proj_dirname.startswith('Project')\
               and not fnmatch(PIname, '[0-9]*'):  # Loop over Project dirs
                   linkname = secondpath + PIname + '/' + dirname + '_' + proj_dirname 
                   list_of_links.append(linkname)
            else:
                dirs_not_checked.append(proj_dirname)
    logging.debug('End get_project_links.')
    with open('dirs_not_checked.out', 'wb') as f:
        f.writelines("\n".join(sorted(dirs_not_checked)))
    logging.info('# of items in flowcell not made into links: %s', len(dirs_not_checked))
    logging.info('# of links to check: %s', len(list_of_links))
    return sorted(list_of_links)


def email_results(missing_dirs, firstpath, secondpath):
    '''Send an email to specified recipients with list of
    missing links.
    '''
    logging.debug('Start email_results.')
    dirs_str = '\n'.join(missing_dirs)
    email_seq = ("Links not found in %s:" % secondpath, dirs_str)
    message = MIMEText("\n".join(email_seq))
    subject = '%i links missing from %s' % (len(missing_dirs), secondpath)
    # ', ' sep list as a single string
    addressees = 'pauline.fujita@ucsf.edu, pauline.fujita@gmail.com'
    send_sge_email.email_list(message, subject, addressees)
    logging.debug('End email_results.')

def main():
    locations, logging_level = handle_cmdline()
    logsetup(logging_level, os.path.splitext(os.path.basename(__file__))[0] + '.log')
    logging.debug('Start main.')
    firstlocation, secondlocation = locations  # unpack user specified paths
    list_of_dirs_found = get_dir_list(firstlocation)  # inital list of dirs
    logging.info('# of directories found in %s: %s', firstlocation,
                 len(list_of_dirs_found))
    dirs_with_complete = complete_file_chk(list_of_dirs_found,
                                            'generic_copy', 'cull', firstlocation)
    logging.info('# of directories found with complete file: %s',
                 len(dirs_with_complete))
    links_to_check = get_project_links(dirs_with_complete, firstlocation, secondlocation)  # secondpath = '/coldstorage/web/ihg-client/'
    links_found, links_missing = dir_exist_chk(links_to_check)
    with open('links_found.out', 'wb') as f:
#        f.writelines("\n".join(sorted(links_found)))
        f.writelines("\n".join(links_found))
    logging.info('# of links found: %s', len(links_found))
    logging.info('# of links that were missing: %s', len(links_missing))
    if len(links_missing) > 0:
        email_results(links_missing, firstlocation, secondlocation)
    logging.debug('End main.')


# code to allow running as module or script...
if __name__ == "__main__":
    main()
