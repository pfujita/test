#!/usr/bin/env python
'''
This program checks for existence of dirs in two locations and
existence of associated casava and rsync completion files.
'''

import os
import os.path
import argparse
import glob
import logging
import smtplib
import send_sge_email
from email.mime.text import MIMEText
from email.Utils import formatdate
from sge_email.models import SGEEmailObject
# from getpass         import getpass
from file_and_dir_comparisons import get_dir_list, complete_file_chk, dir_exist_chk
from log_setup import log_setup


def handle_cmdline():
    """Read in 2 dirs from command line.
    If zero args supplied, return usage msg, or error if valid
    dir names not supplied.
    """

    usage = """
    This program reads in two user supplied paths and looks
    for directories and files common to both locations.

    When verbose flag is given, directories found to pass or fail
    each of the checks are output as a list to a separate file for
    each check. Expected output files:

    full_dir_list.out
    dir_w_rsync.out
    common_dirs.out
    missing_dir_in_fastq.out
    missing_casava_file.out
    """

    # read in command line and options
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(dest='paths', metavar='directory_name', nargs=2,
                        help='Full path to dir w/o trailing slash.')
    parser.add_argument('--log', dest='log_level', action='store',
                        help='log level [CRITICAL, ERROR, WARNING, INFO, DEBUG]')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")
    args = parser.parse_args()
    if args.verbose:
        verbosity = 1
    else:
        verbosity = 0
    return args.paths, args.log_level, verbosity


def write_to_files(reference_path, path_to_check, list_of_dirs, list_of_dirs2,
                   rsync_dirs, common_dirs, missing_dirs, casava_missing):
    ''' 
    Print results from checks at each stage to a files when run in verbose mode.
    '''
    logger = logging.getLogger(logger_name)
    logger.debug('write_to_files.')
    with open('full_dir_list.out', 'wb') as f:
        f.writelines(("Directories found in %s:\n" % reference_path,
                      "\n".join(list_of_dirs), "\n\n"))
    f.closed
    with open('dir_w_rsync.out', 'wb') as f:
        f.writelines(("Directories in %s that had an rsync_complete.txt \
                      file:\n" % reference_path, "\n".join(rsync_dirs), "\n\n"))
    f.closed
    with open('common_dirs.out', 'wb') as f:
        f.writelines(("Directories common to %s and %s:\n" % (reference_path,
                      path_to_check), "\n".join(common_dirs), "\n\n"))
    f.closed
    with open('missing_dir_in_fastq.out', 'wb') as f:
        f.writelines(("Directories found in %s but not in %s:\n" %
                      (reference_path, path_to_check), "\n".join(missing_dirs),
                      "\n\n"))
    f.closed
    with open('missing_casava_file.out', 'wb') as f:
        f.writelines(("Directories in %s that were missing a casava.*.complete\
                      file:\n" % path_to_check, "\n".join(casava_missing)))
    f.closed
    logger.debug('write_to_files.')


def print_to_log(reference_path, path_to_check, list_of_dirs, rsync_dirs,
                 list_of_dirs2, missing_dirs, casava_missing):
    '''
    Print summary stats of results to log.
    '''
    logger = logging.getLogger(logger_name)
    logger.debug('print_to_log.')
    logger.info('# of directories found in %s: %s', reference_path,
                len(list_of_dirs))
    logger.info('# of directories found in %s that had an rsync file:%s',
                reference_path, len(rsync_dirs))
    logger.info('# of directories found in %s: %s', path_to_check,
                len(list_of_dirs2))
    logger.info('# of directories found %s that were not found in %s: %s',
                reference_path, path_to_check, len(missing_dirs))
    logger.info('# of directories found in %s without a casava file: %s',
                path_to_check, len(casava_missing))
    logger.debug('print_to_log.')


def email_results(missing_dirs, reference_path, path_to_check):
    '''
    Send an email to specified recipients with list of missing dirs.
    '''
    logger = logging.getLogger(logger_name)
    logger.debug('email_results.')
    dirs_str = '\n'.join(missing_dirs)
    email_seq = ("Directories found in %s that were not found in %s:" %
                 (reference_path, path_to_check), dirs_str)
    message = MIMEText("\n".join(email_seq))
    subject = '%i dirs missing from fastq' % len(missing_dirs)
    # ', ' sep list as a single string
    addressees = 'pauline.fujita@ucsf.edu'
    send_sge_email.email_list(logger_name, message, subject, addressees)
    logger.debug('email_results.')


def main():
    locations, logging_level, write_to_file = handle_cmdline()
    logger = log_setup(logging_level,
                       os.path.splitext(os.path.basename(__file__))[0] + '.log',
                       logger_name)
    logger.debug('Start main.')
    reference_directory, directory_to_check = locations  # unpack user specified paths
    dirs_in_reference = get_dir_list(logger_name, reference_directory)
    dirs_in_target = get_dir_list(logger_name, directory_to_check)
    dirs_with_rsync = complete_file_chk(logger_name, dirs_in_reference,
                                        'rsync', 'cull', reference_directory)
    common_dirs_found, missing_dirs_found = dir_exist_chk(logger_name,
                                                          dirs_with_rsync,
                                                          directory_to_check)
    dirs_missing_casava = complete_file_chk(logger_name, common_dirs_found,
                                            'casava', 'missing',
                                            directory_to_check)
    if len(missing_dirs_found) > 0:
        email_results(missing_dirs_found, reference_directory, directory_to_check)
    print_to_log(reference_directory, directory_to_check, dirs_in_reference,
                 dirs_with_rsync, dirs_in_target, missing_dirs_found,
                 dirs_missing_casava)
    if write_to_file == 1:
        write_to_files(reference_directory, directory_to_check, dirs_in_reference,
                       dirs_in_target, dirs_with_rsync, common_dirs_found,
                       missing_dirs_found, dirs_missing_casava)
    logger.debug('End main.')


if __name__ == "__main__":
    logger_name = os.path.splitext(os.path.basename(__file__))[0]
    main()
