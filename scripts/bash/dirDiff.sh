#!/usr/bin/env bash

DIRPLACE1="/coldstorage/hiseq-data"
DIRPLACE2="/coldstorage/fastq"
RSYNCPLACE="rsync_complete.txt"

cd $DIRPLACE1 # grab list of dirs
ls -d * > /home/sequencing/pauline_temp/hiseq.dirs
cd /home/sequencing/pauline_temp # return to dir w write perm
for OUTPUT in $(cat hiseq.dirs) # loop over dirs in hiseq
do
   if [[ -e $DIRPLACE1/$OUTPUT/$RSYNCPLACE ]]; then # only check dirs w rsync file
      printf "$DIRPLACE1/$OUTPUT/$RSYNCPLACE\n" >> rsync_found.bash.out
         if [[ -e $DIRPLACE2/$OUTPUT ]]; then # check for dir in fastq
            printf "$DIRPLACE2/$OUTPUT\n" >> dir_found.bash.out
         else
            printf "$DIRPLACE2/$OUTPUT\n" >> dir_missing.bash.out
         fi 
   else
      printf "$DIRPLACE1/$OUTPUT/$RSYNCPLACE\n" >> rsync_not_found.bash.out
   fi
done
