import os
import os.path
import shutil
import fileinput

pathtofix = '/coldstorage/fastq/150805_D00108_0374_AC7GHNANXX/Project_kriegsteina-combo-48'
#pathtofix = '/home/pfujit/fujitap/scripts/python/md5fix'
#md5template = '/coldstorage/fastq/150805_D00108_0374_AC7GHNANXX/Project_costelloj-JO71/Sample_RAD211N/md5_check_sum.sh'
md5template = '/home/sequencing/pauline_temp/md5_check_sum.sh'
md5filename = 'md5_check_sum.sh'

def get_dir_list(pathtofix):
    '''Generate a working list of directories. 
    '''
    # Get all dirs
    list_of_dirs = [name for name in os.listdir(pathtofix)
        	    if os.path.isdir(os.path.join(pathtofix, name))]
    #return sorted(list_of_dirs)
    return list_of_dirs

def main():
    dirs_to_fix = get_dir_list(pathtofix)
#    print "dirs_to_fix", dirs_to_fix
    for thing in dirs_to_fix: 
        shutil.copy(md5template, thing) # copies md5 script over to each dir
        samplename = thing[7:] # removes "Sample_" from beginning (lstrip doesn't like _s)
        newpath = pathtofix + '/' + thing + '/' + md5filename
        print 'qsub -V %s' % newpath
        for line in fileinput.FileInput(newpath, inplace=1):
            line = line.replace('RAD211N', samplename)
            print line,
main()
