import os
import os.path
import pprint
import re
import mmap
from collections import OrderedDict
from compare_files_and_dirs import handle_cmdline, get_dir_list, complete_file_chk, dir_exist_chk, email_list


def limit_dirs(dirs_to_chk, matchterm):  # just 2013 dirs

    for dirname in dirs_to_chk:
        matched_dirs = []
        filelist = glob.glob('*' + matchterm + '*')
        if filelist:
            matched_dirs.append(dirname)
    return matched_dirs


def file_chk(dirs_to_chk, base_path): # dirs w/ and w/o zcat
    '''Cull list of directories from base path to only include
    those with relevant *complete* file, or find directories
    missing the relevant *complete* file.
    '''
    dirs_w_file = []
    dirs_missing_file = []
    for dirname in dirs_to_chk:
        path_to_chk = os.path.join(base_path, dirname)
        filelist = glob.glob(path_to_chk + '/' + 'zcat.sh')
        if not filelist:
            dirs_missing_file.append(dirname)
        if filelist:
            # if os.path.exists(path_to_chk + filename):
            dirs_w_file.append(dirname)
        return sorted(dirs_w_file), sorted(dirs_missing_file)


def parse_zcat(dirs_with_zcat):  # get fastq locations from zcat.sh files
    testpat = re.compile('/coldstorage/fastq/[a-zA-Z0-9-_]*/
                         [a-zA-Z0-9-_]*/[a-zA-Z0-9-_]*.fastq.gz')
    fastq_map = OrderedDict()  # create a map back to fastq
    zcat_missing_fastq = []  # list to store zcats w/o fast ref
    for zcatdir in dirs_with_zcat:
        file_to_search = os.path.join(zcatdir, 'zcat.sh')
        with open (file_to_search,'r+') as f:
            data = mmap.mmap(f.fileno(), 0)  # load whole file as str
            fastq_path = testpat.findall(data)[0]  # return 1st match
#           os.path.dirname(fastq_path)  # strip fastq filename
            if fastq_path:
                fastq_dir = os.path.dirname(fastq_path)  # strip fastq filename
            else:
                zcat_missing_fastq.append(zcatdir)
            fastq_map[zcatdir] = fastq_dir
    return fastq_map, zcat_missing_fastq


def main():
    locations = handle_cmdline()
    firstlocation, secondlocation = locations  # unpack user specified paths
    list_of_dirs_found = get_dir_list(firstlocation)  # inital list of dirs
    match_dirs = limit_dirs(list_of_dirs_found, matchterm)  # matchterm is year limit search
    dirs_with_zcat, dirs_missing_zcat = file_chk(match_dirs, firstlocation)
    map_to_fastq, dirs_w_zcat_missing_fastq = parse_zcat(dirs_with_zcat)


    links_to_check = get_project_links(dirs_with_complete, firstlocation, secondlocation)  # secondpath = '/coldstorage/web/ihg-client/'
    links_found, links_missing = dir_exist_chk(links_to_check)
#   pprint.pprint(links_missing)
    print_results(list_of_dirs_found, dirs_with_complete, links_found, links_missing, links_to_check)
    if len(links_missing) > 0:
        email_list(links_missing, firstlocation, secondlocation)
