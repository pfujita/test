import logging


def log_setup(log_level, log_file_name, name):
    '''
    Configure logger, call with name, set logging level to
    log_level, and write to log_file_name,
    '''
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % log_level)

    logger = logging.getLogger(name)
    logger.setLevel(numeric_level)

    fh = logging.FileHandler(log_file_name)
    fh.setLevel(numeric_level)

    ch = logging.StreamHandler()
    ch.setLevel(logging.ERROR)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s: %(message)s',
                                  datefmt='%m/%d/%Y %I:%M:%S %p')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(fh)
    logger.addHandler(ch)

    return logger
