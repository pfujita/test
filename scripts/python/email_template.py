import smtplib
from email.mime.text import MIMEText
from email.header    import Header
from getpass         import getpass

textfile = "email.test"
fromaddr = 'user@gmail.com'
toaddrs  = 'user@ucsf.edu'

#Gmail login
username, password = 'user@gmail.com', getpass('Gmail password:') # prompt for passwd
#username = 'user@gmail.com'
#password = 'password'

# Write a message from a file:
fp = open(textfile, 'rb')

# Create body of email, simple case set msg = str 
# msg = 'New msgs are neat!'
# msg = MIMEText(fp.read())
a = ['variable 1', 'variable 2']
msg = MIMEText('''
long blocks
of text
here.
''', a)
fp.close()

# msg headers
recipient = "user@ucsf.edu"
msg['Subject'] = 'TEST The contents of %s' % textfile
#msg['From'] = username
#msg['To'] = recipient

server = smtplib.SMTP("smtp.gmail.com:587")
server.starttls()
server.login(username,password)
server.sendmail(fromaddr, toaddrs, msg.as_string()) # last field 'msg' for simple
server.quit()


