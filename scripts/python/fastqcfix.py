import os
import os.path
import shutil
import fileinput
from pipeDiff import handle_cmdline, get_dir_list, complete_file_chk, dir_exist_chk, email_list

pathtofix = '/coldstorage/fastq/150805_D00108_0374_AC7GHNANXX/Project_kriegsteina-combo-48'
#fastqtemplate = '/coldstorage/fastq/150805_D00108_0374_AC7GHNANXX/Project_costelloj-JO71/Sample_RAD211N/fastqc/fastqc.sh'
fastqtemplate = '/home/sequencing/pauline_temp/fastqc.sh'
fastqfilename = 'fastqc.sh'


def handle_cmdline():
    """Read in dir from command line.
    If zero args supplied, return usage msg, or error if valid
    dir name not supplied.
    """

    usage = """
    This program 
    """

    # read in command line and options
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(dest='path', metavar='directory_name', nargs=1,
                        help='Full path to dir w/o trailing slash.\
                              Can use "." to specify current directory.')
    parser.add_argument("-v", "--verbose", help="increase output verbosity for debugging",
                        action="store_true")
    parser.add_argument('--mode', dest='mode', action='store',
                        choices={'md5','fastqc','both'}, default='both',
                        help='type of scripts to run, runs all if none specified'

    args = parser.parse_args()
    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    if args.path[0] == '.':  # to allow use of '.' for cwd
        args.path[0] = os.getcwd()
    return args.path, args.mode, log_level  # , args.outfile


def logsetup(log_level, log_file_name):
    '''Log events to compare_files_and_dirs.log
    '''
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename=log_file_name,
                        level=log_level)


def main():
    pathtofix, run_mode, logging_level = handle_cmdline()
    logsetup(logging_level, 'fastqcfix.log')
    dirs_to_fix = get_dir_list(pathtofix)
    for sampledir in dirs_to_fix: 
        newpath = pathtofix + '/' + sampledir + '/fastqc/'
        os.mkdir(newpath)
        shutil.copy(fastqtemplate, newpath) # copies fastqc script over to each dir
        samplename = sampledir[7:] # removes "Sample_" from beginning (lstrip doesn't like _s)
        new_file_loc = pathtofix + '/' + sampledir + '/fastqc/' + fastqfilename
        print 'qsub -V %s' % new_file_loc
        for line in fileinput.FileInput(new_file_loc, inplace=1):
            line = line.replace('OLDSAMPLE', samplename)
            print line,

    for thing in dirs_to_fix: 
        shutil.copy(md5template, thing) # copies md5 script over to each dir
        samplename = thing[7:] # removes "Sample_" from beginning (lstrip doesn't like _s)
        newpath = pathtofix + '/' + thing + '/' + md5filename
        print 'qsub -V %s' % newpath
        for line in fileinput.FileInput(newpath, inplace=1):
            line = line.replace('RAD211N', samplename)
            print line,
            
main()
