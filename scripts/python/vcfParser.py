#!/usr/bin/env python

'''
A parser for Variant Call Format (VCF) Version 4.1 file. Details of format
specification can be found at: http://samtools.github.io/hts-specs/VCFv4.1.pdf

This program parses the component metadata, header, and data lines of a vcf
file into python objects (lists and dictionaries). It also reassembles these
objects into an output vcf file for comparison to the original input. It also
prints the parsed objects as well as sample queries on these objects to the
terminal.
'''

import csv
import argparse
import pprint
import sys


def handle_cmdline():
    """Read in vcf filename from command line.
    If zero args supplied, return usage msg, or error if valid vcf
    filename not supplied.
    """

    usage = """ Reads in a vcf file and parses into python objects.
                Outputs reassembled vcf file 'parsed_output.vcf
                for comparison.
                """

    # read in command line and options
    parser = argparse.ArgumentParser(description=usage)
    parser.add_argument(dest='filename', metavar='VCFfilename')
    parser.add_argument('-o', dest='outfile', action='store',
                        help='output file')

    args = parser.parse_args()
    return args.filename, args.outfile


def parse_meta_data(line):  # reads in meta_data separately
    """Read meta_data into the meta_data dict."""
    a = (line.lstrip('##')).rstrip('\n')  # strips ## and \n
    b = a.partition('=')  # break into triple ('variable','=','meta_data')
    metadataline = (b[0], b[2])  # stores metadata as list of strings
    return metadataline


def parse_vcf_file(vcf_filename):
    """Read VCF file into meta_data and sample_data."""
    with open(vcf_filename, 'rb') as f:
        metalines = []  # List of key, value pairs from metadata lines
        sample_data = []  # List of dicts, one dict/row
        for line in f:
            if line.startswith('##'):
                metalines.append(parse_meta_data(line))
            elif line.startswith('#'):
                fields = line.split()
                f_tsv = csv.DictReader(f, fieldnames=fields,
                                       delimiter='\t')
                for row in f_tsv:
                    sample_data.append(row)
        return metalines, fields, sample_data
    f.closed


def parse_genotypes(parse_tree, headers):  # further parsing genotype info
    genotypes_master = []
    for row in parse_tree:  # loop over data rows in vcf file
        # create a tuple from the format field of a given row
        formatinfo = row['FORMAT'].split(':')
        genotypes = {}
        for j in range(9, len(headers)):  # loop over samples
            geno_tuple = row[headers[j]].split(':')
            testdict = {}
            for k, m in map(None, formatinfo, geno_tuple):
                testdict[k] = m
            genotypes[headers[j]] = testdict
        genotypes_master.append(genotypes)
    return genotypes_master


def print_parse_tree(metadata, headers, parse_tree, parsed_genotypes):
    """Prints out parsed VCF file as well as sample test queries."""

    # print read out of meta_data and header
    print "metadata:"
    pprint.pprint(metadata)

    print "headers:"
    pprint.pprint(headers)

    # print parsed data block
    print "Parsed data lines:"
    pprint.pprint(parse_tree)

    # print genotype data
    print "Parsed genotype data from sample columns:"
    pprint.pprint(parsed_genotypes)

    # print sample queries of data structures
    print "Example queries on data structures:"
    print "sample_data: row 2, FORMAT column"
    pprint.pprint(parse_tree[2]['FORMAT'])

    print "genotypes_master[row 1][sample NA00001]"
    pprint.pprint(parsed_genotypes[1]['NA00001'])


def reconstruct_and_write_vcf_file(metadata, headers, parse_tree, outfile):
    """Reassembles sample_data rows into a VCF"""
    headers_tuple = tuple(headers)  # csv.DictReader requires hashable keys
    with open(outfile, 'wb') as f:
        for line in metadata:   # restore format to metadata
            metaline = '##' + '='.join(line) + "\n"
            f.write(metaline)
        f_tsv = csv.DictWriter(f, headers_tuple, delimiter="\t",
                               lineterminator='\n')
#           f_tsv.writeheader() # writeheader is new in python v2.7
        headerline = '\t'.join(headers_tuple) + "\n"
        f.write(headerline)
        f_tsv.writerows(parse_tree)
    f.closed


# Main
def main():
    vcf_filename, outfile = handle_cmdline()  # input/output vcf filenames
    metadata, headers, parse_tree = parse_vcf_file(vcf_filename)
    parsed_genotypes = parse_genotypes(parse_tree, headers)
    print_parse_tree(metadata, headers, parse_tree, parsed_genotypes)
    reconstruct_and_write_vcf_file(metadata, headers, parse_tree, outfile)

main()

# code to allow running as module or script...
# if __name__ == "__main__":
#       main(sys.argv)
