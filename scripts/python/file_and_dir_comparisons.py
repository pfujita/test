#!/usr/bin/env python
'''
This program checks for existence of dirs in two locations and
existence of associated casava and rsync completion files.
'''

import os
import os.path
import glob
import logging


def get_dir_list(logger_name, firstpath, filter_term=None):
    '''Generate a list of directories found in a specified path.
    '''
    module_logger = logging.getLogger(logger_name + '.' + __name__)
    module_logger.debug('get_dir_list.')
    # Get all dirs
    list_of_dirs = [name for name in os.listdir(firstpath)
                    if os.path.isdir(os.path.join(firstpath, name))]
    if filter_term is not None:
        filtered_list_of_dirs = [dir for dir in list_of_dirs
                                 if dir.startswith(filter_term)]
        return sorted(filtered_list_of_dirs)
    else:
        return sorted(list_of_dirs)


def complete_file_chk(logger_name, dirs_to_chk, testname, testtype,
                      base_path=None):
    '''Check directories in list for presence of a *complete* file.
    testname = type of complete file (rsync, casava, etc)
    testtype = cull/missing (dirs with/dirs without complete file)
    base_path is for optional reconstruction of full dir paths.
    '''
    module_logger = logging.getLogger(logger_name + '.' + __name__)
    module_logger.debug('complete_file_chk.')
    dirs_w_file = []
    dirs_missing_file = []
    for dirname in dirs_to_chk:
        if base_path is None:
            path_to_chk = dirname
        else:
            path_to_chk = base_path + '/' + dirname
        filelist = glob.glob(path_to_chk + '/' + testname + '*complete*')
        if not filelist:
            dirs_missing_file.append(dirname)
        if filelist:
            dirs_w_file.append(dirname)
    if testtype == 'cull':
        return sorted(dirs_w_file)
    elif testtype == 'missing':
        return sorted(dirs_missing_file)


def dir_exist_chk(logger_name, dirs_to_chk, base_path=None):
    '''Check existence of dirs in a list. If base_path given
    will prepend base_path to dirs in list and check resultant
    full path.
    '''
    module_logger = logging.getLogger(logger_name + '.' + __name__)
    module_logger.debug('dir_exist_chk.')
    # Get list of corresponding dirs in second location
    found_dirs = []  # dirs found in both locations
    missing_dirs = []  # dirs present in 1st location, missing from 2nd
    for dirname in dirs_to_chk:
        if base_path is None:
            fullpath = dirname
        else:
            fullpath = base_path + '/' + dirname
        if os.path.lexists(fullpath):  # lexists bc don't have perm for os.stat
            found_dirs.append(dirname)
        else:
            missing_dirs.append(dirname)
    return sorted(found_dirs), sorted(missing_dirs)
