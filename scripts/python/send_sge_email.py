import logging
import smtplib
from email.Utils import formatdate
from sge_email.models import SGEEmailObject


def email_list(logger_name, message, subject, addressees=None):
    '''Send an email using sge settings.
    Expects addressees to be a ', ' separated string of email addresses.
    '''
    module_logger = logging.getLogger(logger_name + '.' + __name__)
    module_logger.debug('email_list.')
    msg = message
    msg['Subject'] = subject
    msg['Date'] = formatdate(localtime=True)
    sendto = addressees  # if None, sends to list in sge_email/models.py
    sgeemail = SGEEmailObject(subject=msg['Subject'], message=msg.as_string(),
                              recipients=sendto)
    try:
        # server = smtplib.SMTP("smtp.gmail.com:587") # gmail settings
        # server.starttls() #Settings to send from gmail
        s = smtplib.SMTP_SSL(sgeemail.host, 465)
        s.set_debuglevel(1)
        s.login(sgeemail.usrname, sgeemail.password)
        s.sendmail(sgeemail.usrname + '@' + sgeemail.domain,
                   sgeemail.recipients, msg.as_string())
        s.quit()

    except:
        print "Could not send email"
        module_logger.error("from logger! Could not send email")
