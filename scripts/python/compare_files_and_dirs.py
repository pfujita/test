#!/usr/bin/env python
'''
This program checks for existence of dirs in two locations and
existence of associated casava and rsync completion files.
'''

import os
import os.path
import argparse
import glob
import logging
import smtplib
import send_sge_email
from email.mime.text import MIMEText
from email.header import Header
from email.Utils import formatdate
from sge_email.models import SGEEmailObject
# from getpass         import getpass


def handle_cmdline():
    """Read in dir from command line.
    If zero args supplied, return usage msg, or error if valid
    dir name not supplied.
    """

    usage = """
    This program reads in two user supplied paths and looks
    for existence of directories and files in the second location.
    Directories found to pass or fail each of the checks are
    output as a list to a separate file for each check. Expected
    output files:

    full_dir_list.out
    dir_w_rsync.out
    common_dirs.out
    missing_dir_in_fastq.out
    missing_casava_file.out
    """

    # read in command line and options
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(dest='paths', metavar='directory_name', nargs=2,
                        help='Full path to dir w/o trailing slash.\
                              Can use "." for first path.')
    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    if args.verbose:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO
    if args.paths[0] == '.':  # to allow use of '.' for cwd
        args.paths[0] = os.getcwd()
    return args.paths, log_level  # , args.outfile


def logsetup(log_level, log_file_name):
    '''Log events to compare_files_and_dirs.log
    '''
    logging.basicConfig(format='%(asctime)s %(levelname)s:%(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p',
                        filename=log_file_name,
                        level=log_level)


def get_dir_list(firstpath):
    '''Generate a list of directories found in first user specified
    location.
    '''
    # Get all dirs
    logging.debug('Start get_dir_list.')
    list_of_dirs = [name for name in os.listdir(firstpath)
                    if os.path.isdir(os.path.join(firstpath, name))]
    return sorted(list_of_dirs)
    logging.debug('End get_dir_list.')


def complete_file_chk(dirs_to_chk, testname, testtype, base_path=None):
    '''Check directories in list for presence of a *complete* file.
    testname specifies type of complete file. When testtype is set
    to cull, returns only dirs with file, when set to missing, returns
    dirs that are missing *complete* file.
    '''
    logging.debug('Start complete_file_chk.')
    dirs_w_file = []
    dirs_missing_file = []
    for dirname in dirs_to_chk:
        if base_path is None:
            path_to_chk = dirname
        else:
            path_to_chk = base_path + '/' + dirname
        filelist = glob.glob(path_to_chk + '/' + testname + '*complete*')
        if not filelist:
            dirs_missing_file.append(dirname)
        if filelist:
            dirs_w_file.append(dirname)
    if testtype == 'cull':
        return sorted(dirs_w_file)
    elif testtype == 'missing':
        return sorted(dirs_missing_file)
    logging.debug('End complete_file_chk.')


def dir_exist_chk(dirs_to_chk, base_path=None):
    '''Check for existence fo dirs in a list. If base_path given
    will prepend base_path to dirs in list and check resultant
    full path.
    '''
    logging.debug('Start dir_exist_chk.')
    # Get list of corresponding dirs in second location
    found_dirs = []  # List of dirs found to both locations
    missing_dirs = []  # List of dirs present in 1st location, missing from 2nd
    for dirname in dirs_to_chk:
        if base_path is None:
            fullpath = dirname
        else:
            fullpath = base_path + '/' + dirname
        if os.path.lexists(fullpath):  # lexists bc don't have perm for os.stat
            found_dirs.append(dirname)
        else:
            missing_dirs.append(dirname)
    return sorted(found_dirs), sorted(missing_dirs)
    logging.debug('End dir_exist_chk.')


def write_to_files(firstpath, secondpath, list_of_dirs, list_of_dirs2,
                   rsync_dirs, common_dirs, missing_dirs, casava_missing):
    '''Print results from checks at each stage to a file.
    '''
    logging.debug('Start write_to_files.')
    with open('full_dir_list.out', 'wb') as f:
        f.writelines(("Directories found in %s:\n" % firstpath,
                      "\n".join(list_of_dirs), "\n\n"))
    f.closed
    with open('dir_w_rsync.out', 'wb') as f:
        f.writelines(("Directories in %s that had an rsync_complete.txt \
                      file:\n" % firstpath, "\n".join(rsync_dirs), "\n\n"))
    f.closed
    with open('common_dirs.out', 'wb') as f:
        f.writelines(("Directories common to %s and %s:\n" % (firstpath,
                      secondpath), "\n".join(common_dirs), "\n\n"))
    f.closed
    with open('missing_dir_in_fastq.out', 'wb') as f:
        f.writelines(("Directories found in %s but not in %s:\n" %
                      (firstpath, secondpath), "\n".join(missing_dirs),
                      "\n\n"))
    f.closed
    with open('missing_casava_file.out', 'wb') as f:
        f.writelines(("Directories in %s that were missing a casava.*.complete\
                      file:\n" % secondpath, "\n".join(casava_missing)))
    f.closed
    logging.debug('End write_to_files.')


def print_to_log(firstpath, secondpath, list_of_dirs, rsync_dirs,
                 list_of_dirs2, missing_dirs, casava_missing):
    logging.debug('Start print_to_log.')
    # All dirs in first location
    logging.info('# of directories found in %s: %s', firstpath,
                 len(list_of_dirs))
    # Dirs with rsync file
    logging.info('# of directories found in %s that had an rsync file:%s',
                 firstpath, len(rsync_dirs))
    # All dirs in second location
    logging.info('# of directories found in %s: %s', secondpath,
                 len(list_of_dirs2))
    # Dirs not found in second location (that passed rsync test)
    logging.info('# of directories found %s that were not found in %s: %s',
                 firstpath, secondpath, len(missing_dirs))
    # Dirs in second location with a casava file
    logging.info('# of directories found in %s without a casava file: %s',
                 secondpath, len(casava_missing))
    logging.debug('End print_to_log.')


def email_results(missing_dirs, firstpath, secondpath):
    '''Send an email to specified recipients with list of
    missing dirs.
    '''
    logging.debug('Start email_results.')
    dirs_str = '\n'.join(missing_dirs)
    email_seq = ("Directories found in %s that were not found in %s:" %
                 (firstpath, secondpath), dirs_str)
    message = MIMEText("\n".join(email_seq))
    subject = '%i dirs missing from fastq' % len(missing_dirs)
    # ', ' sep list as a single string
    addressees = 'pauline.fujita@ucsf.edu'
    send_sge_email.email_list(message, subject, addressees)
    logging.debug('End email_results.')


# alternate email function that relies on a modified version of
# sge_email/script.py
# def alt_email_list(missing_dirs, firstpath, secondpath):
#    subject = 'my test email string!'
#    # Create body of email, simple case set msg = str
#    dirs_str = '\n'.join(missing_dirs)
#    email_seq = ("Directories found in %s that were not found in %s:" %
#                 (firstpath, secondpath), dirs_str, "\nOutput files in\
#                 /home/sequencing/pauline_temp/pipeDiff_output")
#    message = "\n".join(email_seq)
#    send_list = 'pauline.fujita@ucsf.edu'  # , separated string
#    send_email(subject,message,recipients=send_list,files=[],add_text=None,
#               boilerplates='off')


def main():
    locations, logging_level = handle_cmdline()
    logsetup(logging_level, os.path.splitext(os.path.basename(__file__))[0] + '.log')
    logging.debug('Start main.')
    firstlocation, secondlocation = locations  # unpack user specified paths
    list_of_dirs_found = get_dir_list(firstlocation)
    list_of_dirs_found2 = get_dir_list(secondlocation)
    dirs_with_rsync = complete_file_chk(list_of_dirs_found,
                                        'rsync', 'cull', firstlocation)
    common_dirs_found, missing_dirs_found = dir_exist_chk(dirs_with_rsync,
                                                          secondlocation)
    dirs_missing_casava = complete_file_chk(common_dirs_found,
                                            'casava', 'missing',
                                            secondlocation)
#    write_to_files(firstlocation, secondlocation, list_of_dirs_found,
#                  list_of_dirs_found2, dirs_with_rsync, common_dirs_found,
#                  missing_dirs_found, dirs_missing_casava)
    if len(missing_dirs_found) > 0:
        email_results(missing_dirs_found, firstlocation, secondlocation)
    print_to_log(firstlocation, secondlocation, list_of_dirs_found,
                 dirs_with_rsync, list_of_dirs_found2, missing_dirs_found,
                 dirs_missing_casava)
    logging.debug('End main.')

# code to allow running as module or script...
if __name__ == "__main__":
    main()
