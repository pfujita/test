import argparse
import os
import os.path

def handle_cmdline():
    """Instructions, yeah!
    """

    usage = """
    Some stuff here
    """

    # read in command line and options
    parser = argparse.ArgumentParser(description=usage,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument(dest='path', metavar='directory_name', nargs=1,
                        help='Full path to dir w/o trailing slash.')
    args = parser.parse_args()
    return args.path


def list_generator(list_of_users, basepath):
    for userdir in list_of_users:
        path_to_chk = os.path.join(basepath, userdir)
        print "Processing userdir %s ..." % userdir
        print "With path_to_chk %s ..." % path_to_chk
#        flowcell_dirs = os.listdir(path_to_chk)
        flowcell_dirs = [name for name in os.listdir(path_to_chk) if name.startswith('1')]
        pruned_flowcell_dirs = []
        for dir in flowcell_dirs:
            pruned_dir = dir.split('_Project')[0]
            print "Pruned dir: %s" % pruned_dir
            pruned_flowcell_dirs.append(pruned_dir)
        with open('flowcell_list.out', 'a') as f:
            f.writelines(("User %s:\n" % userdir,
                          "\n".join(sorted(set(pruned_flowcell_dirs))), "\n\n"))
        f.closed
        print "Completed user %s ..." % userdir
        

def main():
#    path_to_list = handle_cmdline()
    path_to_list = '/coldstorage/web/ihg-client/'
    print "path_to_list %s:" % path_to_list
#    user_dirs = os.listdir(path_to_list)
    user_dirs = [name for name in os.listdir(path_to_list)\
                    if os.path.isdir(os.path.join(path_to_list, name))]
    sorted_user_dirs = sorted(user_dirs)
    list_generator(sorted_user_dirs, path_to_list)

main()
